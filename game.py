from random import randint

name = input("Hi! What is your name? ")
num_guesses = int(input("How many guesses does the computer get? "))


for guess_num in range(num_guesses):
    guess_num += 1

    year_number = randint(1924, 2004)
    month_number = randint(1, 12)

    print("Guess ", guess_num, ": " , name, " were you born on ", month_number,  " / ", year_number, " ?" )

    response = input("yes or no? ")

    if response == "yes":
        print("I knew it!")
        exit()
    elif guess_num == num_guesses:
        print("I have other things to do. Good bye.")
    else:
        print("Drat! Lemme try again!.")
